﻿using System;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Globalization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Collections;

namespace code
{
    class Program
    {
        string source;

        public Program(string sourceWord)
        {
            if (sourceWord is null)
            {
                throw new ArgumentNullException(nameof(sourceWord));
            }

            if (sourceWord.Equals(string.Empty))
            {
                throw new ArgumentException(nameof(sourceWord));
            }

            this.source = sourceWord;
        }

        public string[] FindAnagrams(string[] strArray)
        {
            char[] sourceChar = this.source.ToCharArray();
            List<char> strCharList = new List<char>();
            List<string> anagrams = new List<string>();
            bool flIsContains = false;
            for (int i = 0; i < strArray.Length; i++)
            {
                // think about how to work with lowwer case an upper
                strCharList.AddRange(string.Join(string.Empty, strArray[i].ToLower()).ToCharArray());
                if (strCharList.Count < sourceChar.Length)
                {
                    strCharList.Clear();
                    continue;
                }
                for (int j = 0; j < sourceChar.Length; j++)
                {
                    flIsContains = strArray[i].Contains(sourceChar[j], StringComparison.OrdinalIgnoreCase) ? true : false;
                    strCharList.Remove(Char.ToLower(sourceChar[j]));
                    if (!flIsContains)
                    {
                        strCharList.Clear();
                        break;
                    }
                }
                if (flIsContains && strCharList.Count == 0)
                    anagrams.Add(strArray[i]);
            }
            return anagrams.ToArray<string>();
        }

        static void Main()
        {
            /*  */
            var sut = new Program("Orchestra");
            string[] a = sut.FindAnagrams(new[] { "Carthorse" });
            foreach (var e in a)
                Console.WriteLine(e);
            Console.ReadLine();
        }
    }
}